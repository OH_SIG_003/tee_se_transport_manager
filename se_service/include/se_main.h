/*
 * Copyright (C) 2022 Huawei Technologies Co., Ltd.
 * Licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#ifndef SE_SERVICE_SE_MAIN_H
#define SE_SERVICE_SE_MAIN_H

#include "tee_defines.h"
#include "se_service.h"

void se_service_info_init(void);

void se_service_cmd(struct se_srv_msg_t *msg, TEE_UUID *uuid, uint32_t sndr_pid, struct se_srv_rsp_t *rsp);
TEE_Result se_task_free_caller(const struct se_srv_msg_t *msg, struct se_srv_rsp_t *rsp,
    const TEE_UUID *uuid);
#endif
